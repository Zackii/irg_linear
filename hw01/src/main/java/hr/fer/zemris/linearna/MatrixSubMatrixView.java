package hr.fer.zemris.linearna;

public class MatrixSubMatrixView extends AbstractMatrix {

	private IMatrix orginal;
	private int[] rowIndexes;
	private int[] colIndexes;

	public MatrixSubMatrixView(IMatrix orginal, int row, int col) {
		if (orginal == null) {
			throw new IllegalArgumentException();
		}
		if (row > orginal.getRowsCount() || col > orginal.getColsCount()) {
			throw new IllegalArgumentException();
		}

		rowIndexes = new int[orginal.getRowsCount() - 1];
		for (int i = 0, index = 0; i < rowIndexes.length; i++) {
			if (i == row) {
				continue;
			}

			rowIndexes[index++] = i;
		}

		colIndexes = new int[orginal.getColsCount() - 1];
		for (int i = 0, index = 0; i < colIndexes.length; i++) {
			if (i == col) {
				continue;
			}

			colIndexes[index++] = i;
		}

	}

	private MatrixSubMatrixView(IMatrix orginal, int[] rowIndexes, int[] colIndexes) {
		if (orginal == null) {
			throw new IllegalArgumentException();
		}
		for (int i = 0, rowsCount = orginal.getRowsCount() - 1; i < rowIndexes.length; i++) {
			if (rowIndexes[i] > rowsCount) {
				throw new IllegalArgumentException();
			}
		}
		for (int i = 0, colsCount = orginal.getColsCount() - 1; i < colIndexes.length; i++) {
			if (colIndexes[i] > colsCount) {
				throw new IllegalArgumentException();
			}
		}

		this.orginal = orginal;
		this.rowIndexes = rowIndexes;
		this.colIndexes = colIndexes;
	}

	@Override
	public int getRowsCount() {
		return rowIndexes.length;
	}

	@Override
	public int getColsCount() {
		return colIndexes.length;
	}

	@Override
	public double get(int row, int col) {
		return orginal.get(rowIndexes[row], colIndexes[col]);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		return orginal.set(rowIndexes[row], colIndexes[col], value);
	}

	@Override
	public IMatrix copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return orginal.newInstance(n, m);
	}

}
