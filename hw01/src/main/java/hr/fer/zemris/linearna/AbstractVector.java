package hr.fer.zemris.linearna;

import static java.lang.Math.*;

import hr.fer.zemris.linearna.exception.IncompatableOperandExcention;
import hr.fer.zemris.linearna.exception.OperationOnlySupportedOn3DVectorException;

public abstract class AbstractVector implements IVector {

	@Override
	public abstract double get(int index);

	@Override
	public abstract IVector set(int index, double value);

	@Override
	public abstract int getDimension();

	@Override
	public IVector copy() {
		return copyPart(getDimension());
	}

	@Override
	public abstract IVector copyPart(int n);

	@Override
	public abstract IVector newInstance(int n);

	@Override
	public IVector add(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) + other.get(i));
		}
		return this;
	}

	@Override
	public IVector nAdd(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}

		IVector vector = newInstance(this.getDimension());

		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector.set(i, this.get(i) + other.get(i));
		}
		return vector;
	}

	@Override
	public IVector sub(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) - other.get(i));
		}
		return this;
	}

	@Override
	public IVector nSub(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}

		IVector vector = newInstance(this.getDimension());

		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector.set(i, this.get(i) - other.get(i));
		}
		return vector;
	}

	@Override
	public IVector scalarMultiply(double scalar) {
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, scalar * get(i));
		}
		return this;
	}

	@Override
	public IVector nScalarMultiply(double scalar) {
		IVector vector = newInstance(this.getDimension());

		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector.set(i, scalar * get(i));
		}
		return vector;
	}

	@Override
	public double norm() {
		double squaresSum = 0;
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			squaresSum += pow(get(i), 2);
		}
		return sqrt(squaresSum);
	}

	@Override
	public IVector normalize() {
		double norm = norm();

		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, get(i) / norm);
		}

		return this;
	}

	@Override
	public IVector nNormalize() {
		double squaresSum = 0;
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			squaresSum += pow(get(i), 2);
		}

		double magnitude = sqrt(squaresSum);
		IVector vector = newInstance(this.getDimension());

		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector.set(i, get(i) / magnitude);
		}

		return vector;
	}

	@Override
	public double cosine(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}
		
		return scalarProduct(other) / (this.norm()*other.norm());
	}

	@Override
	public double scalarProduct(IVector other) {
		if(other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatableOperandExcention();
		}
		
		double scalarProduct = 0;
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			scalarProduct = this.get(i)*other.get(i);
		}
		return scalarProduct;
	}

	@Override
	public IVector nVectorProduct(IVector other) {
		if(this.getDimension() != 3 && other.getDimension() != 3) {
			throw new OperationOnlySupportedOn3DVectorException();
		}

		IVector vector = newInstance(this.getDimension());
		
		vector.set(0, this.get(1)*other.get(2) - this.get(2)*other.get(1));
		vector.set(1, this.get(2)*other.get(0) - this.get(0)*other.get(2));
		vector.set(2, this.get(0)*other.get(1) - this.get(1)*other.get(0));
		
		return vector;
	}

	@Override
	public IVector nFromHomogeneus() {
		double homogeneus = get(getDimension()-1);
		
		IVector vector = newInstance(this.getDimension());
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector.set(i, this.get(i)/homogeneus);
		}
		return vector;
	}

	@Override
	public IMatrix toRowMatrix(boolean liveView) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IMatrix toColumnMatrix(boolean liveView) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] toArray() {
		double[] array = new double[getDimension()];
		
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			array[i] = this.get(i);
		}

		return array;
	}
	
	public String toString(int precision) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			
		}
		
		return sb.toString();
	}
	
	public String toString() {
		return toString(3);
	}

}
