package hr.fer.zemris.linearna;

public class MatrixVectorView extends AbstractMatrix {

	private IVector orginal;
	private boolean asRowMatrix;
	
	public MatrixVectorView(IVector orginal, boolean asRowMatrix) {
		this.orginal = orginal;
		this.asRowMatrix = asRowMatrix;
	}

	@Override
	public int getRowsCount() {
		if(asRowMatrix) {
			return orginal.getDimension();
		} 

		return 0;
	}

	@Override
	public int getColsCount() {
		if(!asRowMatrix) {
			return orginal.getDimension();
		} 

		return 0;
	}

	@Override
	public double get(int row, int col) {
		if(asRowMatrix && col != 0 || !asRowMatrix && row != 0) {
			throw new IllegalArgumentException();
		} 

		if(asRowMatrix) {
			return orginal.get(row);
		} else {
			return orginal.get(col);
		}
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		if(asRowMatrix && col != 0 || !asRowMatrix && row != 0) {
			throw new IllegalArgumentException();
		} 

		if(asRowMatrix) {
			orginal.set(row, value);
		} else {
			orginal.set(col, value);
		}
		
		return this;
	}

	@Override
	public IMatrix copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
