package hr.fer.zemris.linearna;

public class MatrixTransposeView extends AbstractMatrix {
	
	private IMatrix orginal;
	
	public MatrixTransposeView(IMatrix orginal) {
		this.orginal = orginal;
	}

	@Override
	public int getRowsCount() {
		return orginal.getColsCount();
	}

	@Override
	public int getColsCount() {
		return orginal.getRowsCount();
	}

	@Override
	public double get(int row, int col) {
		return orginal.get(col, row);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		return orginal.set(col, row, value);
	}

	@Override
	public IMatrix copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		return this.orginal.newInstance(n, m);
	}

}
