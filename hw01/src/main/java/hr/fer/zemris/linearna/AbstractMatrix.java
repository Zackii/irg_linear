package hr.fer.zemris.linearna;

import hr.fer.zemris.linearna.exception.IncompatableOperandExcention;

public abstract class AbstractMatrix implements IMatrix {

	@Override
	public abstract int getRowsCount();

	@Override
	public abstract int getColsCount();

	@Override
	public abstract double get(int row, int col);

	@Override
	public abstract IMatrix set(int row, int col, double value);

	@Override
	public abstract IMatrix copy();

	@Override
	public abstract IMatrix newInstance(int n, int m);

	@Override
	public IMatrix nTranspose(boolean liveView) {
		if (liveView) {
			return new MatrixTransposeView(this);
		}

		IMatrix matrix = newInstance(getColsCount(), getRowsCount());
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				matrix.set(j, i, this.get(i, j));
			}
		}

		return matrix;
	}

	@Override
	public IMatrix add(IMatrix other) {
		if (other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()) {
			throw new IncompatableOperandExcention();
		}

		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				this.set(i, j, this.get(i, j) + other.get(i, j));
			}
		}

		return this;
	}

	@Override
	public IMatrix nAdd(IMatrix other) {
		if (other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()) {
			throw new IncompatableOperandExcention();
		}

		IMatrix matrix = newInstance(this.getRowsCount(), this.getColsCount());
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				matrix.set(i, j, this.get(i, j) + other.get(i, j));
			}
		}

		return matrix;
	}

	@Override
	public IMatrix sub(IMatrix other) {
		if (other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()) {
			throw new IncompatableOperandExcention();
		}

		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				this.set(i, j, this.get(i, j) - other.get(i, j));
			}
		}

		return this;
	}

	@Override
	public IMatrix nSub(IMatrix other) {
		if (other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()) {
			throw new IncompatableOperandExcention();
		}

		IMatrix matrix = newInstance(this.getRowsCount(), this.getColsCount());
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				matrix.set(i, j, this.get(i, j) - other.get(i, j));
			}
		}

		return matrix;
	}

	@Override
	public IMatrix nMultiply(IMatrix other) {
		if (other == null) {
			throw new IllegalArgumentException();
		}
		if (this.getColsCount() != other.getRowsCount()) {
			throw new IncompatableOperandExcention();
		}

		IMatrix matrix = newInstance(this.getRowsCount(), other.getColsCount());
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = other.getColsCount() - 1; j >= 0; i--) {
				double sum = 0;
				for (int k = this.getColsCount() - 1; k >= 0; k--) {
					sum = this.get(i, k) * other.get(k, j);
				}
				matrix.set(i, j, sum);
			}
		}

		return matrix;
	}

	@Override
	public double determinant() {
		if (getRowsCount() != getColsCount()) {
			throw new NotSquareMatrixException();
		}
		
		return determinant(this, getRowsCount());
	}

	private double determinant(IMatrix A, int n) {
		double determinant = 0;

		if (n == 1) {
			determinant = A.get(0, 0);
		}
		if (n == 2) {
			determinant = A.get(0, 0) * A.get(1, 1) - A.get(0, 1) * A.get(1, 0);
		} else {

			int prefix;
			for (int i = this.getRowsCount() - 1; i >= 0; i--) {

				if (i % 2 == 0) {
					prefix = 1;
				} else {
					prefix = -1;
				}

				IMatrix m = new MatrixSubMatrixView(this, 0, i);
				determinant += prefix * A.get(0, i) * determinant(m, n - 1);
			}

		}

		return determinant;
	}

	@Override
	public IMatrix subMatrix(int n, int m, boolean liveView) {
		if (liveView) {
			return new MatrixSubMatrixView(this, n, m);
		}

		return copy();
	}

	@Override
	public IMatrix nInvert() {
		IMatrix transpose = this.nTranspose(true);
		IMatrix inverse = newInstance(transpose.getRowsCount(), transpose.getColsCount());
		
		double determinant = determinant();
		
		for (int i = transpose.getRowsCount() - 1; i >= 0; i--) {
			for (int j = transpose.getColsCount() - 1; j >= 0; i--) {
				inverse = set(i, j, (1/determinant)*transpose.get(i, j));
			}
		}
		
		return inverse;
	}

	@Override
	public double[][] toArray() {
		double[][] array = new double[getRowsCount()][getColsCount()];

		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; i--) {
				array[i][j] = this.get(i, j);
			}
		}

		return array;
	}

	@Override
	public IVector toVector(boolean liveView) {
		// TODO Auto-generated method stub
		return null;
	}

}
