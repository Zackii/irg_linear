package hr.fer.zemris.linearna;

public interface IVector {

	double get(int index);
	
	IVector set(int index, double value);
	
	int getDimension();
	
	IVector copy();
	
	IVector copyPart(int n);
	
	IVector newInstance(int n);
	
	IVector add(IVector other);
	
	IVector nAdd(IVector other);
	
	IVector sub(IVector other);
	
	IVector nSub(IVector other);
	
	IVector scalarMultiply(double scalar);
	
	IVector nScalarMultiply(double scalar);
	
	double norm();
	
	IVector normalize();
	
	IVector nNormalize();
	
	double cosine(IVector other);
	
	double scalarProduct(IVector other);
	
	IVector nVectorProduct(IVector other);
	
	IVector nFromHomogeneus();
	
	IMatrix toRowMatrix(boolean liveView);
	
	IMatrix toColumnMatrix(boolean liveView);
	
	double[] toArray();
	
}
