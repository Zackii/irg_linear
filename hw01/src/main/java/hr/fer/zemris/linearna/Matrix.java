package hr.fer.zemris.linearna;

public class Matrix extends AbstractMatrix {

	protected double[][] elements;
	protected int rows;
	protected int cols;

	public Matrix(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		elements = new double[rows][cols];
	}

	public Matrix(int rows, int cols, double[][] elements, boolean copyArray) {
		this.rows = rows;
		this.cols = cols;

		if (copyArray) {
			this.elements = new double[rows][cols];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					this.elements[i][j] = elements[i][j];
				}
			}
		} else {
			this.elements = elements;
		}

	}

	@Override
	public int getRowsCount() {
		return rows;
	}

	@Override
	public int getColsCount() {
		return cols;
	}

	@Override
	public double get(int row, int col) {
		if(row >= rows || col >= cols) {
			throw new IllegalArgumentException();
		}
		
		return elements[row][col];
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		if(row >= rows || col >= cols) {
			throw new IllegalArgumentException();
		}
		
		elements[row][col] = value;
		return this;
	}

	@Override
	public IMatrix copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IMatrix newInstance(int n, int m) {
		double[][] elements = new double[n][m];
		return new Matrix(n, m, elements, false);
	}
	
	public static IMatrix parseSimple(String s) {
		if(s == null) {
			throw new IllegalArgumentException();
		}
		
		String[] ss = s.split("|");
		int rows = ss.length;
		int cols = 0;
		try {
			String row = ss[0].trim();
			cols = row.split(" +").length;
		} catch(ArrayIndexOutOfBoundsException | NumberFormatException exc) {
			throw new IllegalArgumentException();
		}
		
		double[][] elements = new double[rows][cols];

		try {
			
			for(int i = 0; i < ss.length; i++) {
				String[] row = ss[0].trim().split(" +");
				
				for(int j = 0; j < row.length; j++) {
					elements[i][j] = Double.parseDouble(row[j]);
				}
				
			}
			
		} catch(ArrayIndexOutOfBoundsException | NumberFormatException exc) {
			throw new IllegalArgumentException();
		}
		
		return new Matrix(rows, cols, elements, false);
	}

}
