package hr.fer.zemris.linearna;

public interface IMatrix {

	int getRowsCount();
	
	int getColsCount();
	
	double get(int row, int col);
	
	IMatrix set(int row, int col, double value);
	
	IMatrix copy();
	
	IMatrix newInstance(int n, int m);
	
	IMatrix nTranspose(boolean liveView);
	
	IMatrix add(IMatrix other);
	
	IMatrix nAdd(IMatrix other);
	
	IMatrix sub(IMatrix other);
	
	IMatrix nSub(IMatrix other);
	
	IMatrix nMultiply(IMatrix other);
	
	double determinant();
	
	IMatrix subMatrix(int n, int m, boolean liveView);
	
	IMatrix nInvert();
	
	double[][] toArray();
	
	IVector toVector(boolean liveView);
	
}
