package hr.fer.zemris.linearna;

public class VectorMatrixView extends AbstractVector {

	private IMatrix orginal;
	private int dimension;
	private boolean rowMatrix;
	
	public VectorMatrixView(IMatrix orginal) {
		this.orginal = orginal;
		if(orginal.getRowsCount() != 0) {
			rowMatrix = true;
			dimension = orginal.getRowsCount();
		} else {
			dimension = orginal.getColsCount();
		}
	}
	
	@Override
	public double get(int index) {
		if(rowMatrix) {
			return orginal.get(index, 0);
		} else {
			return orginal.get(0, index);
		}
	}

	@Override
	public IVector set(int index, double value) {
		if(rowMatrix) {
			orginal.set(index, 0, value);
		} else {
			orginal.set(0, index, value);
		}
		
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copyPart(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IVector newInstance(int n) {
		// TODO Auto-generated method stub
		return null;
	}

}
