package hr.fer.zemris.linearna;

public class Vector extends AbstractVector {
	
	private double elements[];
	private int dimension;
	private boolean readOnly;
	
	public Vector(double ... elements) {
		this(false, true, elements);
	}
	
	public Vector(boolean readOnly, boolean copyArray, double ... elements) {
		
		this.readOnly = readOnly;
		dimension = elements.length;
		
		if(copyArray) {
			this.elements = new double[dimension];
			for(int i = 0; i < dimension; i++) {
				this.elements[i] = elements[i];
			}
		} else {
			this.elements = elements;
		}
	}

	@Override
	public double get(int index) {
		if(index < 0 || index > dimension-1) {
			throw new IllegalArgumentException();
		}
		return elements[index];
	}

	@Override
	public IVector set(int index, double value) {
		if(readOnly) {
			throw new UnsupportedOperationException();
		}
		if(index < 0 || index > dimension-1) {
			throw new IllegalArgumentException();
		}
		
		elements[index] = value;
		
		return this;
	}
	
	public IVector add(IVector other) {
		if(readOnly) {
			throw new UnsupportedOperationException();
		}
		return super.add(other);
	}
	
	public IVector sub(IVector other) {
		if(readOnly) {
			throw new UnsupportedOperationException();
		}
		return super.sub(other);
	}
	
	public IVector scalarMultiply(double scalar) {
		if(readOnly) {
			throw new UnsupportedOperationException();
		}
		return super.scalarMultiply(scalar);
	}
	
	public IVector normalize() {
		if(readOnly) {
			throw new UnsupportedOperationException();
		}
		return super.normalize();
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copyPart(int n) {
		double[] copyelements = new double[n];
		for(int i = 0; i < dimension; i++) {
			copyelements[i] = elements[i];
		}

		return new Vector(copyelements);
	}

	@Override
	public IVector newInstance(int n) {
		double[] elements = new double[n];
		return new Vector(elements);
	}
	
	public static IVector parseSimple(String s) {
		if(s == null) {
			throw new IllegalArgumentException();
		}
		
		s = s.trim();
		String[] ss = s.split(" +");
		double[] elements = new double[ss.length];
		try {
			
			for(int i = 0; i < ss.length; i++) {
				elements[i] = Double.parseDouble(ss[i]);
			}
			
		} catch(NumberFormatException exc) {
			throw new IllegalArgumentException();
		}
		
		return new Vector(elements);
	}
	

}
